 /**
     * 根据模板生成word  excel 并且上传到minio 中
     *
     * @param bussType  业务类型：中文简称 tzgg、回执单hzd
     * @param bussTypeName  业务类型名称 通知公告、通知公告
     * @param fileType  文件类型：docx和xlsx
     * @param map   模板参照
     * @param url   模板路径
     * @param isPdf  0:word 1:pdf 2:word和pdf   --例如需要界面编辑的就是0 不需要编辑就是1 例如回执单就是1 审计公告就是0
     * @param billId   业务id  以后做版本查询
     */
    public Map<String,String > uploadCreateFile(String bussType, String bussTypeName, String fileType, Map map, String url,String isPdf,String billId){
        Map<String,String> rtnMap = new HashMap<String,String>();
        String rtnFileId = "";
        String filePdfId = "";  // pdf文件id
        isPdf = CommonUtil.isNull(isPdf); // 0:word 1:pdf 2:word和pdf
        try {
            String uuid = CommonUtil.getUUID();
            String urls = sjxmProperties.getProfile() + "/" + uuid;  // 文件上传路径
            //创建文件夹
            String fileName = "/" + bussTypeName;
            File file = new File(urls);
            if (!file.exists()) {
                file.mkdirs();
            }
            String fileUploadPath = "";  // word文档地址
            if(CommonUtil.isNull(fileType).equals("docx")){
                // -------------word
                fileUploadPath = urls + fileName + ".docx";  // word文档地址
                XWPFDocument doc = WordExportUtil.exportWord07(url, map);
                //输出文件
                FileOutputStream outs = new FileOutputStream(new File(fileUploadPath));
                doc.write(outs);
                outs.flush();
                outs.close();

            }else if(CommonUtil.isNull(fileType).equals("xlsx")){
                //---------------excel
                fileUploadPath = urls + fileName + ".xlsx";  // word文档地址
                TemplateExportParams params = new TemplateExportParams(url);
                Workbook workbook = ExcelExportUtil.exportExcel(params, map);
                //输出文件
                FileOutputStream outs = new FileOutputStream(new File(fileUploadPath));
                workbook.write(outs);
                outs.flush();
                outs.close();
            }

            if (!fileUploadPath.equals("")){
                String filePdfPath = urls + fileName + ".pdf";  // pdf生成地址

                //File转MultipartFile
                File tempFile = new File(fileUploadPath);
                FileInputStream fileInputStream = new FileInputStream(tempFile);
                MultipartFile multipartFile = new MockMultipartFile(tempFile.getName(), tempFile.getName(),
                        ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);

                // 附件上传
                MultipartFile[] files = null;
                FileInputStream fileInputStreamPdf = null;

                if(isPdf.equals("0")){
                    files = new MultipartFile[1];
                    files[0] = multipartFile;
                } else if(isPdf.equals("1")) {
                    files = new MultipartFile[1];

                    if(CommonUtil.isNull(fileType).equals("docx")){
                        //word 转pdf
                        WordToPdf wordToPdf = new WordToPdf();
                        wordToPdf.change(fileUploadPath, filePdfPath);
                    }else{
                        // excel 转pdf
                        ExeclToPDF excelToPdf = new ExeclToPDF();
                        excelToPdf.change(fileUploadPath, filePdfPath);
                    }
                    //File转MultipartFile --pdf
                    File pdfFile = new File(filePdfPath);
                    fileInputStreamPdf = new FileInputStream(pdfFile);
                    MultipartFile multipartFilePdf = new MockMultipartFile(pdfFile.getName(), pdfFile.getName(),
                            ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStreamPdf);

                    files[0] = multipartFilePdf;

                }else if(isPdf.equals("2")) {
                    files = new MultipartFile[2];
                    files[0] = multipartFile;

                    if(CommonUtil.isNull(fileType).equals("docx")){
                        //word 转pdf
                        WordToPdf wordToPdf = new WordToPdf();
                        wordToPdf.change(fileUploadPath, filePdfPath);
                    }else{
                        // excel 转pdf
                        ExeclToPDF excelToPdf = new ExeclToPDF();
                        excelToPdf.change(fileUploadPath, filePdfPath);
                    }
                    //File转MultipartFile --pdf
                    File pdfFile = new File(filePdfPath);
                    fileInputStreamPdf = new FileInputStream(pdfFile);
                    MultipartFile multipartFilePdf = new MockMultipartFile(pdfFile.getName(), pdfFile.getName(),
                            ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStreamPdf);

                    files[1] = multipartFilePdf;
                }


                RetData r = uploadFiles(files);
                List<Map<String, String>> resultMaps = (List<Map<String, String>>) r.getData();
                for (int z = 0; z < resultMaps.size(); z++) {
                    Map<String, String> tempMaps = resultMaps.get(z);
                    String tempRtnFild = tempMaps.get("id");
                    if (tempRtnFild.equals("-1")){
                        tempRtnFild = "";
                    }
                    if(isPdf.equals("0")){
                        rtnFileId = tempRtnFild; //word
                    }else if(isPdf.equals("1")){
                        filePdfId = tempRtnFild; //pdf
                    }else if(isPdf.equals("2")){
                        if (z == 0) {
                            rtnFileId = tempRtnFild;
                        } else if (z == 1) {
                            filePdfId = tempRtnFild;
                        }
                    }
                }

                fileInputStream.close();
                if(!isPdf.equals("0")){
                    fileInputStreamPdf.close();
                    File linshiFile2 = new File(filePdfPath);
                    if (linshiFile2.exists()) {
                        linshiFile2.delete();
                    }
                }

                //上传成功后删除临时文件
                File linshiFile = new File(fileUploadPath);
                if (linshiFile.exists()) {
                    linshiFile.delete();
                }

                if (file.exists()) {
                    file.delete();
                }

            }
        }catch (Exception e) {
            e.printStackTrace();
        }

        rtnMap.put("rtnFileId",rtnFileId);
        rtnMap.put("filePdfId",filePdfId);
        return rtnMap;

    }