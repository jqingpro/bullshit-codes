/**
* 上古时期的工具函数，可以理解为什么要定义这些函数，但是这个第一行`var D=document;`真是的是一言难尽！
*/

var D=document;
function $id(id){return typeof id==='string'?D.getElementById(id):id}
function $tag(el,tag){return $id(el).getElementsByTagName(tag)}
var getDocumentHeight=function(){return Math.max(D.documentElement.scrollHeight,D.documentElement.clientHeight)}
var isIE = (D.all) ? true : false;