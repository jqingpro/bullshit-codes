    /**
     * 业务需求：调用第三方接口，把结果存入缓存。
     */
    @Cacheable(key = "#userId")
    @Override
    public List<ResourceEntity> userResources(String userId) {
        List<ResourceEntity> resourceEntities = new ArrayList<>();
        // 调用第三方接口...
        JSONObject parseObject = JSONObject.parseObject(data);
        if(parseObject.getInteger("code") == 200){
            List list = JSONObject.parseObject(JSONObject.toJSONString(parseObject.get("xxx")), List.class);
            for (Object item: list) {
                ResourceEntity resourceEntity = new ResourceEntity();
                JSONObject.parseObject(JSONObject.toJSONString(ckeCluster));
                item.setId(parseObject.getString("ckecluster_id"));
                item.setName(parseObject.getString("cluster_name"));
                resourceEntities.add(resourceEntity);
            }
        }
        // 即使第三方接口出错了，我也不会抛出异常，数据虽然少了，但是我才不会有影响呢。
        return resourceEntities;
    }	